import argparse
import time

from life_bench import game
from life_bench import game_numpy


def create_game(implementation, width, height, gpu):
    if implementation == 'baseline':
        return game.GameBaseline(width, height)
    elif implementation == 'numpy':
        return game_numpy.GameNumpy(width, height, gpu=gpu)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--implementation', type=str, default='baseline',
        help='The implementation to benchmark: baseline|numpy .')
    parser.add_argument('--steps', type=int, default=10,
                        help='Steps to run.')
    parser.add_argument('--width', type=int, default=100,
                        help='Width of the grid.')
    parser.add_argument('--height', type=int, default=100,
                        help='Height of the grid.')
    parser.add_argument('--gpu', type=int, default=None,
                        help='Using GPU')
    args = parser.parse_args()
    game_inst = create_game(args.implementation,
                            args.width, args.height, args.gpu)
    start = time.time()
    game_inst.run(args.steps)
    spent = time.time() - start
    _ = game_inst.get_values()

    print('Ran %s@%dx%d for %d steps took %.2f s. %.2f fps. %.2f Mop/s.' % (
        args.implementation, args.width, args.height, args.steps,
        spent, args.steps / spent,
        args.steps * args.width * args.height / 1_000_000 / spent))
