from abc import ABC, abstractmethod
import random


POSITIVE_RATE = 0.1


class Game(ABC):

    @abstractmethod
    def __init__(self, width, height, values=None):
        assert width >= 1
        assert height >= 1
        if values is not None:
            assert len(values) == height
            assert len(values[0]) == width
        self._width = width
        self._height = height

    @abstractmethod
    def run(self, step=1):
        pass

    @abstractmethod
    def get_values(self):
        pass


class GameBaseline(Game):
    def __init__(self, width, height, values=None):
        super().__init__(width, height, values)

        if values is not None:
            self._values = [[False] * width for i in range(height)]
            for i, row in enumerate(values):
                for j, cell in enumerate(row):
                    self._values[i][j] = cell
        else:
            self._values = []
            for row in range(height):
                self._values.append(
                    random.choices(
                        [True, False],
                        weights=[POSITIVE_RATE, 1.0 - POSITIVE_RATE],
                        k=width))

    def run(self, step=1):
        for s in range(step):
            new_values = [[0] * self._width for i in range(self._height)]

            for r in range(self._height):
                for c in range(self._width):
                    new_values[r][c] = self._next_value(r, c)

            self._values = new_values

    def get_values(self):
        return self._values

    def _next_value(self, r, c):
        neighbors = 0
        for r1 in range(r - 1, r + 2):
            for c1 in range(c - 1, c + 2):
                if (r1 >= 0 and r1 < self._height
                        and c1 >= 0 and c1 < self._height):
                    if (c1 != c or r1 != r) and self._values[r1][c1]:
                        neighbors += 1
        if self._values[r][c]:
            return neighbors == 2 or neighbors == 3
        else:
            return neighbors == 3
