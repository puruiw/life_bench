import unittest

import numpy as np

from life_bench import game
from life_bench import game_numpy


class GameTestCase(unittest.TestCase):
    def setUp(self):
        self._game_cls = game.GameBaseline
        self._additional_args = {}

    def test_3x3(self):
        game_inst = self._game_cls(
            3, 3,
            [[0, 0, 0],
             [0, 1, 0],
             [0, 0, 0]],
            **self._additional_args)

        game_inst.run(1)
        self.assert_numpy_equal(game_inst.get_values(),
                                [[0, 0, 0],
                                 [0, 0, 0],
                                 [0, 0, 0]])

        game_inst = self._game_cls(
            3, 3,
            [[1, 1, 0],
             [0, 1, 0],
             [0, 1, 0]],
            **self._additional_args)

        game_inst.run(1)
        self.assert_numpy_equal(game_inst.get_values(),
                                [[1, 1, 0],
                                 [0, 1, 1],
                                 [0, 0, 0]])

    def test_random(self):
        game_inst = self._game_cls(3, 3, **self._additional_args)
        if self._game_cls != game.GameBaseline:
            ref_game = game.GameBaseline(3, 3, game_inst.get_values())

        game_inst.run(1)
        if self._game_cls != game.GameBaseline:
            ref_game.run(1)
            self.assert_numpy_equal(
                game_inst.get_values(), ref_game.get_values())
        else:
            _ = game_inst.get_values()

    def assert_numpy_equal(self, a, b):
        return np.testing.assert_equal(
            np.array(a, dtype=np.int8), np.array(b, dtype=np.int8))


class GameNumpyTestCase(GameTestCase):
    def setUp(self):
        super().setUp()
        self._game_cls = game_numpy.GameNumpy


class GameNumpyGpuTestCase(GameTestCase):
    def setUp(self):
        super().setUp()
        self._game_cls = game_numpy.GameNumpy
        self._additional_args = {'gpu': 1}


if __name__ == '__main__':
    unittest.main()
