import numpy as np
import cupy as cp

from life_bench import game as base_game


class GameNumpy(base_game.Game):
    def __init__(self, width, height, values=None, gpu=None):
        super().__init__(width, height, values=values)

        if gpu is not None:
            self._np = cp
        else:
            self._np = np
        self._gpu = gpu

        if values is not None:
            self._values = self._np.array(values)
        else:
            self._values = (self._np.random.random(size=(height, width))
                            < base_game.POSITIVE_RATE)

    def run(self, step=1):
        for s in range(step):
            neighbor_count = self._np.zeros(shape=(self._height, self._width),
                                            dtype=self._np.int8)

            neighbor_count[1:, 1:] += self._values[:-1, :-1]
            neighbor_count[1:, :] += self._values[:-1, :]
            neighbor_count[1:, :-1] += self._values[:-1, 1:]

            neighbor_count[:, 1:] += self._values[:, :-1]
            neighbor_count[:, :-1] += self._values[:, 1:]

            neighbor_count[:-1, 1:] += self._values[1:, :-1]
            neighbor_count[:-1, :] += self._values[1:, :]
            neighbor_count[:-1, :-1] += self._values[1:, 1:]

            self._values = np.logical_or(
                neighbor_count == 3,
                np.logical_and(neighbor_count == 2, self._values))

    def get_values(self):
        if self._gpu is not None:
            return self._values.get()
        else:
            return self._values
