#include "game_baseline.h"

#include <algorithm>

namespace life_bench {

namespace {
int alive(const int8 neighbors, const int8 cell) {
  return int8(neighbors == 3 || (neighbors == 2 && cell == 1));
}
}  // namespace

GameBaseline::GameBaseline(const int width, const int height,
                           const int8 *values, const Config &config)
    : Game(width, height, values, config) {
  _values = new int8[height * width];
  if (values != nullptr) {
    std::copy(values, values + height * width, _values);
  }
}

GameBaseline::~GameBaseline() { delete _values; }

void GameBaseline::run(int steps) {
  for (int s = 0; s < steps; ++s) {
    int8 *next_values = new int8[_height * _width];

    for (int row = 0; row < _height; ++row) {
      for (int col = 0; col < _width; ++col) {
        int8 neighbors = 0;
        if (row > 0) {
          if (col > 0) neighbors += cell(row - 1, col - 1);
          neighbors += cell(row - 1, col);
          if (col < _width - 1) neighbors += cell(row - 1, col + 1);
        }

        if (col > 0) neighbors += cell(row, col - 1);
        if (col < _width - 1) neighbors += cell(row, col + 1);

        if (row < _height - 1) {
          if (col > 0) neighbors += cell(row + 1, col - 1);
          neighbors += cell(row + 1, col);
          if (col < _width - 1) neighbors += cell(row + 1, col + 1);
        }

        next_values[row * _width + col] = alive(neighbors, cell(row, col));
      }
    }

    _values = next_values;
  }
}

const int8 *GameBaseline::get_values() { return _values; }

}  // namespace life_bench