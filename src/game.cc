#include "game.h"

namespace life_bench {

Game::Game(const int width, const int height, const int8* values,
           const Config& config)
    : _width(width), _height(height), _config(config) {}

}  // namespace life_bench