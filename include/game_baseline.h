#ifndef LIFE_BENCH_GAME_BASELINE_H
#define LIFE_BENCH_GAME_BASELINE_H

#include "game.h"

namespace life_bench {

class GameBaseline : public Game {
 public:
  GameBaseline(const int width, const int height, const int8* values,
               const Config& config);
  ~GameBaseline();
  virtual void run(int steps);
  virtual const int8* get_values();

 private:
  int8 cell(int row, int col) { return _values[row * _width + col]; }

  int8* _values;

  GameBaseline(const GameBaseline&) = delete;
  GameBaseline& operator=(const GameBaseline&) = delete;
};

}  // namespace life_bench
#endif  // LIFE_BENCH_GAME_BASELINE_H
