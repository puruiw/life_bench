#ifndef LIFE_BENCH_GAME_H
#define LIFE_BENCH_GAME_H

namespace life_bench {

typedef char int8;

struct Config {
  int gpu_index = -1;
};

class Game {
 public:
  Game(const int width, const int height, const int8* values,
       const Config& config);
  ~Game() {}
  virtual void run(int steps) = 0;
  virtual const int8* get_values() = 0;

 protected:
  int _width;
  int _height;
  const Config _config;

 private:
  Game(const Game&) = delete;
  Game& operator=(const Game&) = delete;
};

}  // namespace life_bench

#endif  // LIFE_BENCH_GAME_H